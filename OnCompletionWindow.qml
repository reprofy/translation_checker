import QtQuick 2.0
import QtQuick.Window 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import lessonManager.myqt.org 1.0 as MyLessonMngr

Rectangle {
    id: completionWindow
    visible: true
    ColumnLayout {
        anchors.fill: parent
        Text {
            id: completedText
            text: qsTr("COMPLETED!!!")
            font.bold: true
            font.pointSize: 20
            color: "green"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
        }
        LessonProgressBar {
            id: lessonProgress
            radiusOfCircle: 3*pxInMm
            filledNumber: MyLessonMngr.MyLessonMngr.currentProgress
            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
        }
        Button {
            id: furtherButton
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            focus: true
            implicitHeight: 10*pxInMm
            text: qsTr("OK")
            function nextWindow() {
                MyLessonMngr.MyLessonMngr.endLesson()
                topLoader.sourceComponent = mainComponent
            }

            onClicked: {
                nextWindow();
            }
            Keys.onReturnPressed: {
                nextWindow();
            }
        }

    }
    Timer {
        id: beforeStartDelay
        interval: 500
        repeat: false
        onTriggered: {
            lessonProgress.changeColor(MyLessonMngr.MyLessonMngr.currentProgress+1, "green")
        }
    }

    Component.onCompleted:
    {
        beforeStartDelay.running = true
    }

}
