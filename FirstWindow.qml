﻿import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Window 2.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQml.Models 2.3
import lessonManager.myqt.org 1.0 as MyLessonMngr


Window {
    title: qsTr("Choose your lesson")
    width: 480
    height: 240
    visible: true
    id: mainWindow

    property int pxInMm: Math.floor(Screen.pixelDensity)  //to define sizes in millimeters
    property int i;
    property bool firstStart: true;
    property int firstLangIndex;
    property int secondLangIndex;

    function refreshListModel() {
        listModel.clear();
        i = 0;
        for (i = 0; i < MyLessonMngr.MyLessonMngr.lessonPreviewsForQml.length; i++) {
            listModel.append(MyLessonMngr.MyLessonMngr.lessonPreviewsForQml[i]);
        }
    }

    function startLesson () {
        console.log ("Staring new lesson")
        topLoader.source= "LessonWindow.qml"
    }
    ListModel {
        id: listModel
    }
    Loader {
        id: topLoader
        anchors.fill: parent
        sourceComponent: mainComponent
    }
    //This component is for Loader (change windows)
    Component {
        id: mainComponent
        //The first rectangle loaded by Loader
        Rectangle {
            id: mainWindowRect
            anchors.fill: parent
            RowLayout {
                id: rowL
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.rightMargin: 5
                Text {
                    Layout.topMargin: 5
                    Layout.bottomMargin: 5
                    id: actionInvite
                    Layout.alignment: Qt.AlignLeft
                    text: qsTr("Choose lesson:")
                }
                RowLayout {
                    spacing: pxInMm

                    ComboBox {
                        Layout.topMargin: 5
                        Layout.bottomMargin: 5
                        Layout.rightMargin: pxInMm
                        Layout.alignment: Qt.AlignRight
                        id: firstLangButton
                        model: languagePairs
                        implicitHeight: 8*pxInMm
                        horizontalPadding: 2*pxInMm
                        leftPadding: 1*pxInMm
                        onActivated: {
                            firstLangIndex = firstLangButton.currentIndex
                            MyLessonMngr.MyLessonMngr.setLanguageActivated(currentText)
                            MyLessonMngr.MyLessonMngr.setCurrentLanguagePair([currentText, secondLangButton.currentText])
                        }
                        onWindowChanged: {
                            firstLangButton.currentIndex = firstLangIndex
                            secondLangButton.currentIndex = secondLangIndex
                        }
                    }
                    Button {
                        Layout.topMargin: 5
                        Layout.bottomMargin: 5
                        id: orderButton
                        text: "\u21C6"
                        implicitWidth: 5*pxInMm
                        implicitHeight: 8*pxInMm
                        Layout.alignment: Qt.AlignRight
                        visible: false
                        onClicked: {
                            refreshListModel()
                        }
                    }
                    ComboBox {
                        Layout.topMargin: 5
                        Layout.bottomMargin: 5
                        Layout.rightMargin: pxInMm
                        Layout.alignment: Qt.AlignRight
                        id: secondLangButton
                        model: MyLessonMngr.MyLessonMngr.connectedLanguages
                        implicitHeight: 8*pxInMm
                        horizontalPadding: 2*pxInMm
                        leftPadding: 1*pxInMm
                        onActivated: {
                            secondLangIndex = secondLangButton.currentIndex
                            MyLessonMngr.MyLessonMngr.setCurrentLanguagePair([firstLangButton.currentText, currentText])
                        }
                    }
                    Layout.alignment: Qt.AlignRight
                }

            }
            //List of lessons
            ScrollView {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.top: rowL.bottom
                focus: true

                Keys.onDownPressed: listView.incrementCurrentIndex()
                Keys.onUpPressed: listView.decrementCurrentIndex()
                Keys.onReturnPressed: {
                    listView.startLesson()
                }

                ListView {
                    id: listView
                    model: listModel
                    clip: true
                    keyNavigationEnabled: true
                    highlightFollowsCurrentItem: true
                    currentIndex: 0
                    property string currentName: ""
                    function startLesson () {
                        MyLessonMngr.MyLessonMngr.startNewLesson(listView.currentItem.getName())
                        mainWindow.startLesson()
                    }
                    Component {
                        id: listItem
                        Rectangle {
                            id: compRect
                            property bool moreLessVisibility: false
                            height: Math.max(picsLt.height, cl.height, buttonsLayout.height) + 2*pxInMm
                            width: parent.width
                            color: "transparent"
                            border.width: 0.3*pxInMm
                            border.color: "blue"
                            radius: 5
                            z: 5
                            function getName() {
                                return itemName.text
                            }

                            MouseArea {
                                anchors.fill: parent
                                propagateComposedEvents: true
                                onClicked: {listView.currentIndex = index; mouse.accepted=false;}

                            }
                            RowLayout {
                                id: rl
                                anchors.verticalCenter: compRect.verticalCenter
                                anchors.fill: parent
                                //Icon and progress bar are placed in this column
                                ColumnLayout {
                                    id: picsLt
                                    Layout.margins: {
                                        leftMargin: pxInMm
                                        topMargin: pxInMm
                                        bottomMargin: pxInMm
                                    }

                                    Rectangle {
                                        id: imgRect
                                        width: 10*pxInMm
                                        height: 10*pxInMm
                                        Layout.alignment: Qt.AlignHCenter
                                        border.color: "blue"
                                        border.width: 0.5*pxInMm
                                        radius: pxInMm
                                        Image {
                                            id: iconImg
                                            anchors.fill: imgRect
                                            smooth: true
                                            source: iconPath
                                        }
                                    }
                                    LessonProgressBar {
                                        id: userProgress
                                        radiusOfCircle: 1*pxInMm
                                        color: "transparent"
                                        filledNumber: progress
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
                                    }
                                }
                                //Description of lessons
                                ColumnLayout {
                                    id: cl
                                    Layout.alignment: Qt.AlignLeft
                                    Text {
                                        id: itemName
                                        text: name
                                        visible: false
                                    }
                                    Text {
                                        text: firstLanguage+'/'+secondLanguage
                                        visible: false
                                    }
                                    Text {
                                        id: firstLang
                                        text: firstLanguage+': '+firstLangCaption
                                        Layout.alignment: Qt.AlignTop
                                    }

                                    Text {
                                        id: firstLangDesc
                                        text: firstLangDescription
                                        Layout.fillWidth: true
                                        wrapMode: Text.Wrap
                                        visible: moreLessVisibility
                                        leftPadding: 2*pxInMm
                                        rightPadding: 2*pxInMm
                                    }
                                    Text {
                                        id: secondLang
                                        text: secondLanguage+': '+secondLangCaption
                                    }
                                    Text {
                                        id: secondLangDesc
                                        text: secondLangDescription
                                        visible: moreLessVisibility
                                        Layout.fillWidth: true
                                        wrapMode: Text.Wrap
                                        leftPadding: 2*pxInMm
                                        rightPadding: 2*pxInMm
                                    }

                                    onWidthChanged: {
                                        cl.update()
                                    }

                                    Text {
                                        text: iconPath
                                        visible: false
                                    }
                                }

                                // More and Start buttons are placed here
                                ColumnLayout{
                                    id: buttonsLayout
                                    Layout.alignment: Qt.AlignRight
                                    Layout.rightMargin: pxInMm
                                    Button {
                                        id: startButton
                                        implicitHeight: 10*pxInMm
                                        horizontalPadding: 2*pxInMm
                                        text: qsTr("Start")
                                        onClicked: {
                                            listView.currentIndex = index;
                                            if (MyLessonMngr.MyLessonMngr.startNewLesson(name)) {
                                                mainWindow.startLesson()
                                            } else {
                                                console.log("Can't open the lesson", name)
                                            }
                                        }

                                    }
                                    Button {
                                        id: showMoreLess
                                        implicitHeight: 10*pxInMm
                                        horizontalPadding: 2*pxInMm
                                        text: qsTr("More")
                                        onClicked: {
                                            listView.currentIndex = index;
                                            moreLessVisibility = !moreLessVisibility
                                            if (moreLessVisibility !== true)
                                            {
                                                showMoreLess.text = qsTr("More")
                                                rl.height = Math.max(picsLt.height, cl.height, buttonsLayout.height)
                                                rl.update()
                                                console.log("cl height", cl.height, buttonsLayout.height, rl.height, compRect.height )
                                            } else
                                            {
                                                showMoreLess.text = qsTr("Less")
                                                rl.height = Math.max(picsLt.height, cl.height, buttonsLayout.height)
                                                rl.update()
                                                console.log("cl height", cl.height, buttonsLayout.height, rl.height, compRect.height)
                                            }
                                        }
                                    }

                                }
                            }

                        }
                    }
                    Component {
                        id: highlightBar
                        Rectangle {
                            color: "lightsteelblue"
                            radius: 5
                        }

                    }

                    delegate: listItem
                    highlight:  Rectangle { color: "lightsteelblue"; radius: 5; }
                    spacing: 3
                }
            }
            Connections {
                target: mainWindow
                onVisibilityChanged: {
                    if (firstStart){
                        firstLangButton.currentIndex = firstLangButton.find(MyLessonMngr.MyLessonMngr.getFirstLang())
                        MyLessonMngr.MyLessonMngr.setLanguageActivated(firstLangButton.currentText)
                        secondLangButton.currentIndex = secondLangButton.find(MyLessonMngr.MyLessonMngr.getSecondLang())
                        firstLangIndex = firstLangButton.currentIndex
                        secondLangIndex = secondLangButton.currentIndex
                        refreshListModel();
                        firstStart = false;
                    }
                }
            }
        }
    }
    minimumWidth: 320
    minimumHeight: 240
    Connections {
        target: MyLessonMngr.MyLessonMngr
        onLessonPreviewsChanged: {
            refreshListModel();
        }
        onCurrentProgressChanged: {
            refreshListModel();
        }
    }
}

