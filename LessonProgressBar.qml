import QtQuick 2.0
import QtQuick.Layouts 1.3

Rectangle {
    property int radiusOfCircle: 16
    property int topMargin: 1
    property int bottomMargin:1
    property int filledNumber: 0
    property int durationOfChange: 900
    property real amplitudeOfChange: 1.8
    height: radiusOfCircle*2 + topMargin + bottomMargin
    width: rl.width
    RowLayout {
        id: rl
        anchors.horizontalCenter: parent.horizontalCenter
        Rectangle{
            id: circleImg1
            Layout.leftMargin: 5
            Layout.topMargin: topMargin
            Layout.bottomMargin: bottomMargin
            width: radiusOfCircle*2
            height: radiusOfCircle*2
            radius: radiusOfCircle
            border.width: 0.4*pxInMm
            border.color: "green"
            color: ((filledNumber>0) ? "green" : "transparent")
        }
        Rectangle{
            id: circleImg2
            Layout.leftMargin: 1
            Layout.topMargin: topMargin
            Layout.bottomMargin: bottomMargin
            width: radiusOfCircle*2
            height: radiusOfCircle*2
            radius: radiusOfCircle
            border.width: 0.4*pxInMm
            border.color: "green"
            color: ((filledNumber>1) ? "green" : "transparent")
        }
        Rectangle{
            id: circleImg3
            Layout.leftMargin: 1
            Layout.topMargin: topMargin
            Layout.bottomMargin: bottomMargin
            Layout.rightMargin: 5
            width: radiusOfCircle*2
            height: radiusOfCircle*2
            radius: radiusOfCircle
            border.width: 0.4*pxInMm
            border.color: "green"
            color: ((filledNumber>2) ? "green" : "transparent")
        }
    }

    property var array: [circleImg1, circleImg2, circleImg3];


    property Item tmp: circleImg1; // make assignment to supress qml warning
    property int indexToChange;
    property int numberOfCompleted;
    function changeColor(elementIndex, newColor) {
        if (elementIndex <= 3)
        {
            tmp = array[elementIndex-1];
            completionAnimation.running = true;
            completionAnimationSize.running = true;
            completionAnimationRadius.running = true;
            completionAnimationPosX.running = true;
            completionAnimationPosY.running = true;
            completionAnimationBorder.running = true;

        } else
        {
            completionAnimationAllBorders.running = true;
        }
    }
    function setColor(numberOfColored) {
        var step = 0;
        for (step = 0; step<array.length;  step++)
        {
            tmp = array[step];
            if (step < numberOfColored)
            {
                tmp.color = "green";
            }
        }

    }

    PropertyAnimation {
        id: completionAnimation
        target: tmp
        property: "color"
        to: "green"
        duration: 200
    }
    PropertyAnimation {
        id: completionAnimationSize
        target: tmp
        properties: "height,width"
        easing.type: Easing.SineCurve;
        easing.amplitude: 2;
        duration: durationOfChange
        to: radiusOfCircle*2*amplitudeOfChange
    }
    PropertyAnimation {
        id: completionAnimationScale
        target: tmp
        properties: "scale"
        exclude: ["border.width"]
        easing.type: Easing.SineCurve;
        easing.amplitude: 2;
        duration: durationOfChange
        to: amplitudeOfChange
    }
    PropertyAnimation {
        id: completionAnimationRadius
        target: tmp
        properties: "radius"
        easing.type: Easing.SineCurve;
        easing.amplitude: 2;
        duration: durationOfChange
        to: radiusOfCircle*amplitudeOfChange
    }
    PropertyAnimation {
        id: completionAnimationPosX
        target: tmp
        properties: "x"
        easing.type: Easing.SineCurve;
        easing.amplitude: 2;
        duration: durationOfChange
        to: tmp.x-(radiusOfCircle*amplitudeOfChange-radiusOfCircle)
    }
    PropertyAnimation {
        id: completionAnimationPosY
        target: tmp
        properties: "y"
        easing.type: Easing.SineCurve;
        easing.amplitude: 2;
        duration: durationOfChange
        to: tmp.y-(radiusOfCircle*amplitudeOfChange-radiusOfCircle)
    }
    PropertyAnimation {
        id: completionAnimationBorder
        target: tmp
        properties: "border.color"
        easing.type: Easing.SineCurve;
        easing.amplitude: 2;
        duration: durationOfChange
        to: "orange"
    }
    PropertyAnimation {
        id: completionAnimationAllBorders
        targets: array
        properties: "border.color"
        easing.type: Easing.SineCurve;
        easing.amplitude: 2;
        duration: durationOfChange*2
        to: "orange"
    }
    PropertyAnimation {
        id: completionAnimationAllScales
        targets: array
        properties: "scale"
        easing.type: Easing.SineCurve;
        easing.amplitude: 2;
        duration: durationOfChange*2
        to: 1
    }
}
