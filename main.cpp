#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QString>
#include <QSqlQuery>
#include <QDebug>
#include "model/sqldbserver.h"
#include "model/lessonmanager.h"
#include <QQmlEngine>


#include <QtQuick/qquickitem.h>
#include <QtQuick/qquickview.h>
#include <QDir>
#include <QStringList>
#include "model/lessonpreview.h"
#include <iostream>

#include "model/userdatamanager.h"

static QObject *singletontype_provider(QQmlEngine *engine, QJSEngine *scriptEngine)
 {
     Q_UNUSED(engine)
     Q_UNUSED(scriptEngine)
    LessonManager *instance = LessonManager::getInstance();

    QQmlEngine::setObjectOwnership(instance, QQmlEngine::CppOwnership);
    return instance;
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    //Orderer is trying to delete objects in the right order
    Orderer orderer;
    LessonManager * myLessonMngr = orderer.getLM();
    QQmlApplicationEngine * engine = orderer.getEngine();
    qRegisterMetaType<LessonPreview*>("LessonPreview*");
    qmlRegisterSingletonType<LessonManager>("lessonManager.myqt.org", 1, 0, \
                                            "MyLessonMngr", singletontype_provider);
    QQmlContext *ctxt=engine->rootContext();
   // myLessonMngr->setQMLContext(ctxt);
    //QVariant tmp = myLessonMngr->getLessonPreviews();
    //ctxt->setContextProperty("myModel", tmp);
    ctxt->setContextProperty("languagePairs", myLessonMngr->getLanguagePairs());
    engine->load(QUrl(QStringLiteral("qrc:/FirstWindow.qml")));

    int rc = app.exec();
    return rc;
}

//TODO: simple queries
