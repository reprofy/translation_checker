TEMPLATE = app

QT += qml quick widgets sql

SOURCES += main.cpp \
    model/lessonmanager.cpp \
    model/lesson.cpp \
    model/lessonpreview.cpp \
    model/userdatamanager.cpp \
    model/sqldbserver.cpp \
    model/csvdbserver.cpp

RESOURCES += qml.qrc

CONFIG += c++14

QMAKE_CXXFLAGS += -Wall -Werror

#DESTDIR = ../translation_checker/resources/

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    model/lessonmanager.h \
    model/lesson.h \
    model/lessonpreview.h \
    model/userdatamanager.h \
    model/sqldbserver.h \
    model/dbserver.h \
    model/csvdbserver.h

DISTFILES += \
    resources/user/userdata.json
