import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Window 2.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import lessonManager.myqt.org 1.0 as MyLessonMngr

Rectangle {
    id: lessonWindow
    anchors.fill: parent
    function completeLesson () {
        topLoader.source = "OnCompletionWindow.qml"
    }

    Timer {
        id: nextWordTimer
        interval: 1000
        repeat: false
        onTriggered:  {MyLessonMngr.MyLessonMngr.nextWord(answerText.text)}
    }
    Timer {
        id: disappearDelay
        interval: 1000
        repeat: false
        onTriggered: checkResultStatus.visible = false
    }
    Timer {
        id: onCompletionDelay
        interval: 500
        repeat: false
        onTriggered: {
            lessonWindow.completeLesson();
        }
    }
    ColumnLayout {
        width: parent.width - 10
        height: parent.height - 10
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.fill: parent.fill
        Text {
            id: wordForTranslation
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            font.bold: true
            font.pointSize: 20
            text: MyLessonMngr.MyLessonMngr.currentWord
        }

        Text {
            id: checkResultStatus
            PropertyAnimation {
                id: statusAnimation
                target: checkResultStatus
                property: "color"
                to: "Red"
                duration: 25
            }

            Layout.alignment: Qt.AlignHCenter
            font.bold: true
            font.pointSize: 20
            visible: false            
        }

        TextField {
            id: answerText
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            font.pointSize: 16
            horizontalAlignment: TextEdit.AlignHCenter
            placeholderText: qsTr("Insert translation")
            focus: true
            selectByMouse: true
            Keys.onReturnPressed: {MyLessonMngr.MyLessonMngr.nextWord(answerText.text)}
        }
        RowLayout {
            Layout.alignment: Qt.AlignTop
            Button {
                id: showTranslation
                text: qsTr("Show translation")
                Layout.fillWidth: true
                implicitHeight: 10*pxInMm
                onClicked: MyLessonMngr.MyLessonMngr.provideTranslation()
            }
            Button {
                id: checkTheAnswer
                text: qsTr("Check")
                Layout.fillWidth: true
                implicitHeight: 10*pxInMm
                onClicked: {MyLessonMngr.MyLessonMngr.nextWord(answerText.text)}
            }

        }
    }
    Connections {
        target: MyLessonMngr.MyLessonMngr
        onTranslationStateChanged:
        {
            if (MyLessonMngr.MyLessonMngr.translationState === 0)            //state Starting
            {
            }
            else
            if (MyLessonMngr.MyLessonMngr.translationState === 1)            //state Guessing
            {
                checkTheAnswer.text = qsTr("Check the answer");
                if (MyLessonMngr.MyLessonMngr.isAnswered === true)           //switched from state answered
                {
                    nextWordTimer.stop();
                    checkResultStatus.visible = false;
                    answerText.text = "";
                    answerText.focus = true
                    answerText.forceActiveFocus()
                } else                                          //we want to blink with the same text of error
                {
                    checkResultStatus.color = "Orange";
                    statusAnimation.running = true;
                }
            }
            else
            if (MyLessonMngr.MyLessonMngr.translationState === 2)            //state Answered
            {
                checkTheAnswer.text = qsTr("Further");
                nextWordTimer.start();
            } else
            if (MyLessonMngr.MyLessonMngr.translationState === 3)
            {
                onCompletionDelay.start()
            }
        }
        onIsAnsweredChanged:
        {
            if (MyLessonMngr.MyLessonMngr.isAnswered)
            {
                checkResultStatus.text = qsTr("Right!");
                checkResultStatus.color = "Green";
                checkResultStatus.visible = true;
            }
            else
            {
                checkResultStatus.visible = true;
                checkResultStatus.text = qsTr("Wrong answer")
                answerText.selectAll()
                answerText.forceActiveFocus()
                disappearDelay.start()
            }
        }
        onTranslationProvided:
        {
            checkResultStatus.visible = true;
            checkResultStatus.color = "DarkRed";
            checkResultStatus.text = MyLessonMngr.MyLessonMngr.translation
            answerText.focus = true
            answerText.forceActiveFocus()
            answerText.selectAll()
        }
    }


    MessageDialog {
        id: messageDialog
        title: qsTr("You are great")

        function show(caption) {
            messageDialog.text = caption;
            messageDialog.open();
        }
    }
}
