#ifndef USERDATAMANAGER_H
#define USERDATAMANAGER_H

#include <QFile>
#include <QPair>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QObject>
#include <QDateTime>
#include <QVector>

#include <memory>
#include <vector>
#include <map>

typedef struct _LessonProgress LessonProgress;
struct _LessonProgress {
    QString name;
    int progress;
    QDateTime lastDate;
    bool reversed;
};

typedef struct _UserData UserLessonsData;
struct _UserData {
    QString userLabel;
    QString userName;
    QString lastFirstLanguageChoice;
    QString lastSecondLanguageChoice;
    std::map <QString, std::unique_ptr<LessonProgress>> userLessons;
};

enum UserLessonsDataAction {Add, Change, Remove};
class UserDataManager : public QObject
{
    Q_OBJECT
public:
    explicit UserDataManager();
    UserDataManager(const QString &pathToUserData);
    ~UserDataManager(){}

    bool readUserData ();
    bool writeUserData ();
    bool changeUserData (const UserLessonsData *data, UserLessonsDataAction action);
    int getCurrentProgress (const QString lessonName);
    void setNewProgress (const QString lessonName, int newProgress);
    QString getLastFirstLanguage() {
        return userLessonsData.lastFirstLanguageChoice;
    }
    QString getLastSecondLanguage() {
        return userLessonsData.lastSecondLanguageChoice;
    }
    void setCurrentLanguages(QString first, QString second) {
        userLessonsData.lastFirstLanguageChoice = first;
        userLessonsData.lastSecondLanguageChoice = second;
    }

    UserLessonsData userLessonsData;
private:

    Q_DISABLE_COPY(UserDataManager)
    std::unique_ptr<QFile> jsonFile;
    std::unique_ptr<QFile> jsonFileOut;

signals:

public slots:
};

#endif // USERDATAMANAGER_H
