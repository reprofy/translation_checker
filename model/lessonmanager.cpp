﻿#include "lessonmanager.h"


LessonManager::LessonManager():\
    //transfer it later to the body with additional check
    defaultResourcesFolder(":/resources/"),
    numberOfWordsPerLesson(3),
    m_translationState(Guessing),
    answered(false),
    currentTranslation(""),
    dataManager("../translation_checker/resources/user/userdata2.json")
{

    dataManager.readUserData();

    QDir dir(":/resources/dictionaries/");
    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    QStringList sl("*.json");
    dir.setNameFilters(sl);
    QFileInfoList list = dir.entryInfoList();
    for (int i=0; i < list.size(); i++) {
        QFileInfo fileInfo = list.at(i);
        QString newPath (fileInfo.filePath());
        LessonPreview * tempObject = new LessonPreview(newPath);
        int tmpProgress = dataManager.getCurrentProgress(tempObject->getName());
        tempObject->setProgress(tmpProgress);
        lessonPreviewsMap[tempObject->getName()] = tempObject;
        languagePairs[tempObject->getFirstLanguage().language].append\
                (tempObject->getSecondLanguage().language);
        languagePairs[tempObject->getSecondLanguage().language].append\
                (tempObject->getFirstLanguage().language);
        languagePairs[tempObject->getFirstLanguage().language].removeDuplicates();
        languagePairs[tempObject->getSecondLanguage().language].removeDuplicates();
    }
    //TODO: need to rewrite initialization, fixed first lesson?
    QString lessonName("animals_de_ru");
    currentLessonName = lessonName;
    currentLesson = std::unique_ptr<Lesson>(new Lesson());
    connectedLanguages =  QVariant::fromValue(*(languagePairs.begin()));
    //FIXME: add check, languagePairs might be empty
    currentLanguagePair.append(*(languagePairs.keys().begin()));
    currentLanguagePair.append(*(languagePairs.begin()->begin()));

}

LessonManager::~LessonManager()
{
    dataManager.writeUserData();
    for (auto x: lessonPreviewsMap){
        delete x.second;
    }
}

bool LessonManager::startNewLesson(const QString &lessonName)
{
    qDebug() << "The lesson name is " << lessonName;
    currentLessonName = lessonName;
    DBFormat format = lessonPreviewsMap[currentLessonName]->getFormat();
    currentLesson = std::unique_ptr<Lesson>(new Lesson(lessonName, numberOfWordsPerLesson, format, false));
    //TODO: add extra check
    if (!currentLesson->loadLesson())
    {
        qDebug() << "The lesson" << lessonName << "wasn't found";
        return false;
    }
    currentLesson->shuffleWords();
    currentProgress = dataManager.getCurrentProgress(currentLessonName);
    m_translationState = Guessing;
    emit translationStateChanged();
    emit currentWordChanged();
    emit currentProgressChanged();
    return true;

}

void LessonManager::endLesson()
{
    lessonPreviewsMap[currentLessonName]->setProgress\
            (dataManager.getCurrentProgress(currentLessonName));
    emit currentProgressChanged();
}


void LessonManager::setCurrentWord(QString & arg)
{
    Q_UNUSED(arg);
    bool isNotEnded = currentLesson->nextWord();
    if (isNotEnded)
    {
        emit currentWordChanged();
    } else
    {
        m_translationState = LessonIsOver;

        emit translationStateChanged();
    }
}

bool LessonManager::checkResult(QString &arg)
{
   return currentLesson->checkTranslation(arg);
}


QString LessonManager::currentWord()
{
    qDebug() << "Set current word in LM";
    QString word = currentLesson->currentWord();
    return word;
}

LessonManager::TranslationState LessonManager::translationState()
{
    return m_translationState;
}

bool LessonManager::isAnswered()
{
    return answered;
}


void LessonManager::nextWord(QString arg)
{
    //before beginning, obsolete
    if (m_translationState == Starting)
    {
        currentLesson->currentWord();
        m_translationState = Guessing;
    } else
    //user was provided with question
    if (m_translationState == Guessing)
    {
        answered = checkResult(arg);
        //the right answer was given
        if (answered == true)
        {
            m_translationState = Answered;
        } else //the wrong answer provided, user keep guessing
        {
            m_translationState = Guessing;
        }
        emit isAnsweredChanged();
    } else
    //user answered correctly on previous question
    if (m_translationState == Answered)
    {
        m_translationState = Guessing;
        //if there are other questions, then start thinking on a new word
        if (!currentLesson->nextWord())
        {
            /*
             * there are no more words
             * user has complited the lesson
             */
            m_translationState = LessonIsOver;
            dataManager.setNewProgress(currentLessonName, currentProgress+1);
        }
        else
        //there is another word, update the word in a question
        {
            /*
             * go to the next word and notify that it was changed
             */
            emit currentWordChanged();
        }
    }
    /*
     * notify qml that the state was changed
     */
    emit translationStateChanged();
}

void LessonManager::provideTranslation()
{
     currentTranslation = currentLesson->getCurrentTranslation();
     emit translationProvided();
}

void LessonManager::setLanguageActivated(QString lang)
{
    //add some check of lang here
    connectedLanguages =  QVariant::fromValue(languagePairs[lang]);
    //qDebug() << connectedLanguages.toStringList().join(",");
    emit connectedLanguagesChanged();
}

QString LessonManager::translation()
{
    return currentTranslation;
}

void LessonManager::populateLessonPreviews()
{
    lessonPreviews.clear();
    for (auto it: lessonPreviewsMap)
    {
        LessonPreview  * x = it.second;
        if ((x->getFirstLanguageName() == currentLanguagePair[0] &&\
             x->getSecondLanguageName() == currentLanguagePair[1]) ||
            (x->getFirstLanguageName() == currentLanguagePair[1] &&\
                 x->getSecondLanguageName() == currentLanguagePair[0]))
        {
            lessonPreviews.append(x);
        }
    }
    dataManager.setCurrentLanguages(currentLanguagePair[0], currentLanguagePair[1]);
}


void LessonManager::getFirstLangPair()
{

    QString lang1 = LessonManager::dataManager.getLastFirstLanguage();
    QString lang2 = LessonManager::dataManager.getLastSecondLanguage();
    if (LessonManager::languagePairs.contains(lang1) &&\
            LessonManager::languagePairs[lang1].contains(lang2))
    {
        LessonManager::currentLanguagePair.clear();
        LessonManager::currentLanguagePair.append(lang1);
        LessonManager::currentLanguagePair.append(lang2);
    } //else  use values provided during initialization
}
