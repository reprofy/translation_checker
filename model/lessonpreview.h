#ifndef LESSONPREVIEW_H
#define LESSONPREVIEW_H
#include <QFile>
#include <QPair>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QObject>
#include <QDateTime>
#include <QDebug>

typedef struct LanguageDescription LanguageDescription;
struct LanguageDescription {
    QString language;
    QString caption;
    QString description;
};
Q_DECLARE_METATYPE(LanguageDescription)

enum DBFormat {Unknown, CSV, SQLite3};

class LessonPreview: public QObject
{
            Q_OBJECT
    Q_PROPERTY(QString name READ getName NOTIFY nameChanged)
    Q_PROPERTY(QString version READ getVersion NOTIFY versionChanged)
    Q_PROPERTY(QString firstLanguage READ getFirstLanguageName NOTIFY firstLanguageChanged)
    Q_PROPERTY(QString firstLangCaption READ getCaptionFirst NOTIFY firstLangCaptionChanged)
    Q_PROPERTY(QString firstLangDescription READ getDescriptionFirst NOTIFY firstLangDescriptionChanged)
    Q_PROPERTY(QString secondLanguage READ getSecondLanguageName NOTIFY secondLanguageChanged)
    Q_PROPERTY(QString secondLangCaption READ getCaptionSecond NOTIFY secondLangCaptionChanged)
    Q_PROPERTY(QString secondLangDescription READ getDescriptionSecond NOTIFY secondLangDescriptionChanged)
    Q_PROPERTY(QString numberOfWords READ getNum NOTIFY numberOfWordsChanged)
    Q_PROPERTY(QString iconPath READ getIcon NOTIFY iconPathChanged)
    Q_PROPERTY(QDateTime lastDate READ getLastDate NOTIFY lastDateChanged)
    Q_PROPERTY(int progress READ getProgress WRITE setProgress NOTIFY progressChanged)

public:
    LessonPreview() {}
    LessonPreview(QString fileName);
   // LessonPreview(const LessonPreview& other);
    virtual ~LessonPreview(){}
    QString getName() const {
        return name;
    }
    QString getFirstLanguageName() {
        return firstLanguage.language;
    }
    QString getSecondLanguageName() {
        return  secondLanguage.language;
    }
    int getVersion() const {
        return version;
    }
    int getNum () const {
        return numberOfWords;
    }
    QString getIcon () const {
        return iconPath;
    }
    LanguageDescription getFirstLanguage () const {
        return firstLanguage;
    }
    QString getCaptionFirst () const {
        return firstLanguage.caption;
    }
    QString getDescriptionFirst () const {
        return firstLanguage.description;
    }
    QString getCaptionSecond () const {
        return secondLanguage.caption;
    }
    QString getDescriptionSecond () const {
        return secondLanguage.description;
    }
    LanguageDescription getSecondLanguage () const {
        return secondLanguage;
    }
    QDateTime getLastDate () const {
        return lastDate;
    }
    unsigned int getProgress () const {
        return progress;
    }

    void setProgress (int newValue) {
        progress = newValue;
        qDebug() << "Progress is set to " << progress;
        emit progressChanged();
    }

    DBFormat getFormat() {
        return dbFormat;
    }
signals:
    void nameChanged();
    void versionChanged();
    void firstLanguageChanged();
    void firstLangCaptionChanged();
    void firstLangDescriptionChanged();
    void secondLanguageChanged();
    void secondLangCaptionChanged();
    void secondLangDescriptionChanged();
    void numberOfWordsChanged();
    void iconPathChanged();
    void lastDateChanged();
    void progressChanged();
private:
    Q_DISABLE_COPY(LessonPreview)
    QString name;
    int version;
    int numberOfWords;
    QString iconPath;
    LanguageDescription firstLanguage, secondLanguage;
    QDateTime lastDate;
    unsigned int progress;
    DBFormat dbFormat;
};

//Q_DECLARE_METATYPE(LessonPreview)
Q_DECLARE_METATYPE(LessonPreview*)

#endif // LESSONPREVIEW_H
