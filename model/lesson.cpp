#include "lesson.h"
#include <algorithm>
#include <time.h>

Lesson::Lesson() : dbServer(nullptr),\
    currentWordNumber(0), inverted(false)
{
}

Lesson::Lesson(const QString &lessonName, int numberOfWordsPerLesson, DBFormat format, bool isInverted)
{
    currentWordNumber = 0;
    inverted = isInverted;
    if (format == CSV || format == Unknown) {
        dbServer = std::unique_ptr<DBServer>(new CsvDBServer(lessonName, inverted));
    } else if (format == SQLite3) {
        dbServer = std::unique_ptr<DBServer>(new SqlDBServer(lessonName, inverted));
    } else {
        //TODO: throw exception here
        qDebug() << "Unknown DB format";
    }

    numberOfWords = numberOfWordsPerLesson;
}

Lesson::~Lesson()
{

}

bool Lesson::loadLesson()
{
    if (dbServer != nullptr)
    {
        if (!dbServer->openDB())
        {
            qDebug() << "Can't open a database file";
            return false;
        }
    } else
    {
        qDebug() << "DBServer is not initialized";
        return false;
    }

    lessonWords = dbServer->getWords();
    if (lessonWords.length() > 0)
    {
        m_currentWord = lessonWords[currentWordNumber].first;
    } else
    {
        qDebug() << "Empty lesson";
        return false;
    }
    return true;
}

void Lesson::shuffleWords()
{
    std::srand((unsigned)std::time(nullptr));
    vectorMutex.lock();
    std::random_shuffle(lessonWords.begin(), lessonWords.end());
    vectorMutex.unlock();
}

bool Lesson::checkTranslation(QString &translation)
{
    if (translation == lessonWords[currentWordNumber].second)
        return true;
    else
        return false;
}

//TODO: Something wrong here, due to init routine
QString Lesson::currentWord()
{
    return lessonWords[currentWordNumber].first;
}

//TODO: currentWordNumber boundary check is needed
bool Lesson::nextWord()
{
    currentWordNumber++;
    if (currentWordNumber < numberOfWords)
    {
        vectorMutex.lock();
        m_currentWord = lessonWords[currentWordNumber].first;
        vectorMutex.unlock();
        //qDebug() << currentWordNumber << numberOfWords;
        return true;
    } else
    {
//        qDebug() << "Ended";
        return false;
    }
}

 QString Lesson::getCurrentTranslation()
 {
     return lessonWords[currentWordNumber].second;
 }

