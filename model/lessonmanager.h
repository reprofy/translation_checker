#ifndef LESSONMANAGER_H
#define LESSONMANAGER_H

#include <QObject>
#include <QString>
#include <QVariant>
#include <QSqlError>
#include <QQuickView>
#include <QColor>
#include <QDir>
#include <QStringList>
#include <QFileInfo>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QMap>
#include <QDebug>
#include <QList>

#include <memory>
#include <map>

#include "sqldbserver.h"
#include "lesson.h"
#include "lessonpreview.h"
#include "userdatamanager.h"


class LessonManager : public QObject
{
    Q_OBJECT

    Q_ENUMS(TranslationState)
    Q_PROPERTY(QString currentWord READ currentWord WRITE setCurrentWord NOTIFY currentWordChanged)
    Q_PROPERTY(TranslationState translationState READ translationState NOTIFY translationStateChanged)
    Q_PROPERTY(bool isAnswered READ isAnswered NOTIFY isAnsweredChanged)
    Q_PROPERTY(QString translation READ translation NOTIFY translationProvided)
    Q_PROPERTY(int currentProgress READ getCurrentProgress NOTIFY currentProgressChanged)
    Q_PROPERTY(QVariant connectedLanguages READ getConnectedLanguages NOTIFY connectedLanguagesChanged)
    Q_PROPERTY(QString languageActivated READ getLanguageActivated WRITE setLanguageActivated)
    Q_PROPERTY(QVariant lessonPreviewsForQml READ getLessonPreviews NOTIFY lessonPreviewsChanged)
public:
    static LessonManager* getInstance()
    {
        static LessonManager instance;

        return &instance;
    }
    enum TranslationState {Starting, Guessing, Answered, LessonIsOver};

    void initLessonManager();


    void setNextButtonName();
    Q_INVOKABLE bool startNewLesson(const QString &lessonName);
    Q_INVOKABLE void endLesson();

    Q_INVOKABLE QString getFirstLang() {
        getFirstLangPair();
        return currentLanguagePair[0];
    }
    Q_INVOKABLE QString getSecondLang(){
        getFirstLangPair();
        return currentLanguagePair[1];
    }


    int getCurrentProgress () {
        return currentProgress;
    }

    QVariant getLessonPreviews() {
        populateLessonPreviews();
        lessonPreviewsForQml = QVariant::fromValue\
                (lessonPreviews);
        return lessonPreviewsForQml;
    }

    QVariant getLanguagePairs ()  {
        return QVariant::fromValue(QStringList(languagePairs.keys()));
    }

    QVariant getConnectedLanguages() {
        return connectedLanguages;
    }

    QString getLanguageActivated() { return "";}
    ~LessonManager();

    Q_INVOKABLE void setCurrentLanguagePair(QStringList langs)
    {
        currentLanguagePair = langs;
        emit lessonPreviewsChanged();
        qDebug() << currentLanguagePair.join(" ");
    }

    QVariant lessonPreviewsForQml;

private:
    LessonManager (LessonManager const&);
    void operator= (LessonManager const&);
    QList <QObject*> lessonPreviews;
    QMap <QString, QStringList> languagePairs;
    QVariant connectedLanguages;
    LessonManager();
    QString defaultResourcesFolder;
    int numberOfWordsPerLesson;
    //entety of the current lesson
    std::unique_ptr<Lesson> currentLesson;
    TranslationState m_translationState;
    //idicator of the current answer correctness
    bool answered;
    //to show "a hint", translation of the current word
    QString currentTranslation;
    int currentProgress;
    QString currentLessonName;
    std::map <QString, LessonPreview*> lessonPreviewsMap;
    QStringList currentLanguagePair;

    UserDataManager dataManager;

public:
    //check the answer provided by user
    bool checkResult(QString &arg);
    //returns current word to work on
    QString currentWord();
    //returns current game state for qml
    TranslationState translationState();
    //returns the state of the question for qml
    bool isAnswered();
    //FIXME: probably there is no need in it
    QString checkResult();
    //FIXME: is it needed
    QColor colorOfStatus();
    //needed to provide translation to qml
    QString translation();
    void populateLessonPreviews();
    void getFirstLangPair();

signals:
    void currentWordChanged();
    void translationStateChanged();
    void isAnsweredChanged();
    void translationProvided();
    void lessonIsOver();
    void lessonPreviewsChanged();
    void currentProgressChanged();
    void connectedLanguagesChanged();

public slots:
    void nextWord(QString arg);
    void setCurrentWord(QString & arg);
    void provideTranslation();
    void setLanguageActivated(QString lang);
};

class Orderer
{
private:
    LessonManager *lm;
    QQmlApplicationEngine * engine;
public:
    Orderer () {
        lm = LessonManager::getInstance();
        engine = new QQmlApplicationEngine();
    }
    ~Orderer () {
        delete engine;
    }
    QQmlApplicationEngine * getEngine()
    {
        return engine;
    }
    LessonManager * getLM()
    {
        return lm;
    }
};

#endif // LESSONMANAGER_H
