#ifndef DBSERVER_H
#define DBSERVER_H

#include <QObject>
#include <QString>
#include <QVector>
#include <QPair>

class DBServer : public QObject
{
    Q_OBJECT
private:
    DBServer();
    const QString &lessonName;
    bool inverted;
public:
    explicit DBServer(const QString &lessonName, bool isInverted): \
        lessonName (lessonName), inverted(isInverted)
    {

    }


    virtual ~DBServer() = 0;
    //return true on success
    virtual bool openDB() = 0;
    virtual void closeDB() = 0;

    virtual QVector <QPair<QString, QString>> & getWords() = 0;

};

inline DBServer::~DBServer() {}

#endif // DBSERVER_H
