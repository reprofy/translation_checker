#ifndef LESSON_H
#define LESSON_H

#include <QObject>
#include <QSqlQuery>
#include <QList>
#include <QPair>
#include <QVector>
#include <QSqlRecord>
#include <QVariant>
#include <QDebug>
#include <QMutex>
#include <memory>
#include "sqldbserver.h"
#include "csvdbserver.h"
#include "dbserver.h"
#include "lessonpreview.h"


/**
 * @brief The Lesson class
 * @details This class provides an API for manipulating an instance of a
 * current exercise.
 */
class Lesson : public QObject
{
    Q_OBJECT

public:
    Lesson();
    Lesson(const QString &lessonName, int numberOfWordsPerLesson, DBFormat format = CSV, bool isInverted = false);
    ~Lesson();

    /**
     * @brief loadLesson load a new Lesson
     * @return true if the lesson was successfully opened, otherwise false
    */
    bool loadLesson ();

    /**
     * @brief getNextWord returns the next word
     * @return next word
     */
    const QString getNextWord();

    /**
     * @brief checkTranslation checks whether the provided translation is right
     * @param translation the provided translation by user
     * @return whether translation is right
     */
    bool checkTranslation(QString &translation);

    /**
     * @brief currentWord returns current word in the lesson
     * @return current word
     */
    QString currentWord();

    /**
     * @brief nextWord changes the current word and increments the word counter
     * @return true if there is another word, false if there are no (more) words to learn
     */
    bool nextWord();

    /**
     * @brief getCurrentTranslation a hint, returns the right answer to the current word
     * @return the aspected answer
     */
    QString getCurrentTranslation();

    /**
     * @brief shuffleWords place words inside of the vector in a random order
     */
    void shuffleWords();

private:
    Q_DISABLE_COPY(Lesson)
    //to work with the vector of words, lock it first
    QMutex vectorMutex;
    //Database with the current lesson
    std::unique_ptr<DBServer> dbServer;
    //Path to the database
    QString dbPath;
    //Folder containing database
    QString defaultResourcesFolder;
    //query for the current lesson
    QSqlQuery curQuery;
    //the vector keeping word pairs: first language - second language
    QVector < QPair<QString, QString> > lessonWords; //not the best solution
    //number of words in a lesson
    int numberOfWords;
    //current position in lessonWords
    int currentWordNumber;
    //sometimes it keep current word, but sometimes not (in the beginning)
    QString m_currentWord;
    //define what word will be shown as a question on the first language or on the second
    bool inverted;
signals:

public slots:

};

#endif // LESSON_H
