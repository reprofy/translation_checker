#include "lessonpreview.h"
#include <QDebug>


LessonPreview::LessonPreview(QString fileName):QObject(nullptr)
{
    QFile jsonData (fileName);
    if (!jsonData.open(QIODevice::ReadOnly)) {
        qWarning ("Couldn't open the file");
        exit(1);
    }
    QByteArray saveData = jsonData.readAll();
    QJsonDocument loadDoc (QJsonDocument::fromJson(saveData));
    QJsonObject lessonObject (loadDoc.object());
    name = lessonObject["name"].toString();
    version = lessonObject["appversion"].toInt();
    numberOfWords = lessonObject["number_of_words"].toInt();
    iconPath = lessonObject["icon_img"].toString();
    QString format = lessonObject["dbformat"].toString();
    qDebug() << format;
    if (format == "CSV")
    {
        dbFormat = CSV;
    } else if (format == "SQLite3") {
        dbFormat = SQLite3;
    } else {
        dbFormat = Unknown;
    }

    QFile icon (iconPath);
    if (!icon.exists())
        iconPath = "resources/images/icon.png";
    QJsonObject firstLang = lessonObject["first_language"].toObject();
    firstLanguage.language = firstLang["language"].toString();
    firstLanguage.caption = firstLang["caption"].toString();
    firstLanguage.description = firstLang["description"].toString();
    QJsonObject secondLang = lessonObject["second_language"].toObject();
    secondLanguage.language = secondLang["language"].toString();
    secondLanguage.caption = secondLang["caption"].toString();
    secondLanguage.description = secondLang["description"].toString();
}
