#ifndef SQLDBSERVER_H
#define SQLDBSERVER_H

#include <QObject>
#include <QString>
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QFile>
#include <memory>
#include "dbserver.h"

class DBServer;

class SqlDBServer : public DBServer
{
public:
    explicit SqlDBServer(const QString &lessonName, \
                         bool isInverted);
    virtual ~SqlDBServer();
    virtual bool openDB() {return true;}
    virtual void closeDB();
    QVector <QPair<QString, QString>> & getWords()
    {
        return lessonWords;
    }

private:
    QSqlDatabase dbServer;
    //Path to the database
    QString dbPath;
    //Folder containing database
    QString defaultResourcesFolder;
    //query for the current lesson
    QSqlQuery curQuery;
    //the vector keeping word pairs: first language - second language
    QVector < QPair<QString, QString> > lessonWords;
    //order of languages in the lesson
    bool inverted;
signals:

public slots:
};

#endif // SQLDBSERVER_H
