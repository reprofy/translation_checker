#include "csvdbserver.h"
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QPair>

CsvDBServer::CsvDBServer(const QString &lessonName, bool isInverted): DBServer (lessonName, isInverted),\
    defaultResourcesFolder(":resources/db_files/"),\
    inverted(false)
{
    lessonFileName = lessonName + QString(".csv");
    dbPath = defaultResourcesFolder + lessonFileName;
}

bool CsvDBServer::openDB()
{
    QFile dbFile(dbPath);
    if (!dbFile.exists())
    {
        qWarning() << "File "<<dbPath<<"doesn't exist";
        return false;
    } else {
        dbFile.copy("./"+lessonFileName);
        dbPath = lessonFileName;
        QFile::setPermissions(dbPath, QFile::WriteOwner | QFile::ReadOwner);
        qDebug() << lessonFileName << "is copied into the local folder";
    }
    if (!dbFile.open(QFile::ReadOnly))
        return false;
    //QString name("Lesson DB");
    qDebug() << "Try to open" << dbFile.fileName();
    //in order to avoid error "unused private field"
    qDebug() << "Inverted -" << inverted;
    QString oneLine;
    QStringList separatedLine;
    QTextStream fileStream(&dbFile);
    lessonWords.clear();
    while (fileStream.readLineInto(&oneLine))
    {
        separatedLine = oneLine.split(',');
        if (separatedLine.length() >= 2)
        {
            lessonWords.append(qMakePair(separatedLine[0], separatedLine[1]));
        }
    }

    return true;
}
