#ifndef CSVDBSERVER_H
#define CSVDBSERVER_H

#include "dbserver.h"

class DBServer;

class CsvDBServer : public DBServer
{
public:
    explicit CsvDBServer(const QString &lessonName, \
                         bool isInverted);
    virtual ~CsvDBServer() {}
    virtual bool openDB();
    virtual void closeDB() {}


    virtual QVector <QPair<QString, QString>> & getWords()
    {
        return lessonWords;
    }
private:
    //Path to the database
    QString dbPath;
    //Folder containing database
    QString defaultResourcesFolder;
    //the vector keeping word pairs: first language - second language
    QVector < QPair<QString, QString> > lessonWords;
    //order of languages in the lesson
    bool inverted;
    //filename of a lesson file
    QString lessonFileName;
};

#endif // CSVDBSERVER_H
