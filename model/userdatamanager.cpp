#include "userdatamanager.h"
#include "QDebug"
#include "Qt"
#include <memory>
#include <iostream>

UserDataManager::UserDataManager()
{

}

UserDataManager::UserDataManager(const QString &pathToUserData)
{
    //need to improve
    std::unique_ptr<QFile> p(new QFile(pathToUserData));
    jsonFile = std::move(p);
    std::unique_ptr<QFile> t(new QFile(pathToUserData));
    jsonFileOut = std::move(t);
}

bool UserDataManager::readUserData()
{
    if (!jsonFile->open(QIODevice::ReadOnly)) {
        qWarning ("Couldn't open the file");
        return false;
    }
    QByteArray saveData = jsonFile->readAll();
    QJsonDocument loadDoc (QJsonDocument::fromJson(saveData));
    if (loadDoc.isNull()) {
        qWarning ("Wrong JSON document");
        return false;
    }
    QJsonObject userDataObject (loadDoc.object());
    userLessonsData.userLabel = userDataObject["user_label"].toString();
    userLessonsData.userName = userDataObject["user_name"].toString();
    QJsonArray lastLanguages  = userDataObject["last_language_pair_choice"].toArray();
    QJsonArray::const_iterator i;
    i = lastLanguages.begin();
    if (i != lastLanguages.end()) {
        userLessonsData.lastFirstLanguageChoice = i->toString();
    }
    i++;
    if (i != lastLanguages.end()) {
        userLessonsData.lastSecondLanguageChoice = i->toString();
        qDebug() << "I am here" << i->toString();
    }
    QJsonArray userLessonsInfo  = userDataObject["lessons"].toArray();

    for (i = userLessonsInfo.begin(); i < userLessonsInfo.end(); i++) {
        std::unique_ptr<LessonProgress> lessonProgress(new LessonProgress());
        QJsonObject objectTmp = i->toObject();
        lessonProgress->name = objectTmp["lesson_name"].toString();
        lessonProgress->progress = objectTmp["progress"].toInt();
        QString tmpDate = objectTmp["last_date"].toString();

        QDateTime lastDate;
        lastDate = lastDate.fromString(tmpDate, Qt::ISODate);
        lessonProgress->lastDate = lastDate;
        lessonProgress->reversed = objectTmp["reversed"].toBool();
        userLessonsData.userLessons[lessonProgress->name] = std::move(lessonProgress);
    }

    jsonFile->close();
    return true;
}

bool UserDataManager::writeUserData()
{
    if (!jsonFileOut->open(QIODevice::WriteOnly)) {
        qWarning ("Couldn't open the file");
        return false;
    }
    QJsonObject tmpJson;
    tmpJson["user_label"]= userLessonsData.userLabel;
    tmpJson["user_name"] = userLessonsData.userName;
    QJsonArray langPair;
    langPair.append(userLessonsData.lastFirstLanguageChoice);
    langPair.append(userLessonsData.lastSecondLanguageChoice);
    tmpJson["last_language_pair_choice"] = langPair;
    QJsonArray tmpArray;
    for (auto&& tmpObject: userLessonsData.userLessons) {
        QJsonObject tmpJsonObject;
        tmpJsonObject["lesson_name"] = tmpObject.second->name;
        tmpJsonObject["progress"] = tmpObject.second->progress;
        tmpJsonObject["last_date"] = tmpObject.second->lastDate.toString(Qt::ISODate);
        tmpJsonObject["reversed"] = tmpObject.second->reversed;
        tmpArray.append(tmpJsonObject);
    }
    tmpJson["lessons"] = tmpArray;
    QJsonDocument saveDoc(tmpJson);
    jsonFileOut->write(saveDoc.toJson());
    jsonFileOut->close();
    return true;
}

int UserDataManager::getCurrentProgress(const QString lessonName)
{
    auto it = userLessonsData.userLessons.find(lessonName);
    if (it != userLessonsData.userLessons.end()) {
        return userLessonsData.userLessons[lessonName]->progress;
    } else {
        return 0;
    }
}

void UserDataManager::setNewProgress(const QString lessonName, int newProgress)
{
    if (newProgress > 3) {
        return;
    }
    auto it = userLessonsData.userLessons.find(lessonName);
    if (it != userLessonsData.userLessons.end()) {
        userLessonsData.userLessons[lessonName]->progress = newProgress;
        userLessonsData.userLessons[lessonName]->lastDate = QDateTime::currentDateTime();
        qDebug() << "New progress is " << newProgress;
    } else {
        std::unique_ptr<LessonProgress> tmp(new LessonProgress());
        tmp->lastDate = QDateTime::currentDateTime();
        tmp->name = lessonName;
        tmp->progress = newProgress;
        tmp->reversed = false;
        userLessonsData.userLessons[lessonName] =  std::move(tmp);
    }
    writeUserData();
}
