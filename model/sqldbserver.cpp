#include "sqldbserver.h"
#include <QUrl>
#include <QSqlError>

SqlDBServer::SqlDBServer(const QString &lessonName, bool isInverted):  \
        DBServer(lessonName, isInverted), \
        defaultResourcesFolder(":resources/db_files/"),\
        inverted(isInverted)
{
    dbPath = defaultResourcesFolder + lessonName;
    QFile dbFile(dbPath);
    //MAGIC NUMBER!!!
    int numberOfWordsPerLesson = 3;
    if (!dbFile.exists())
    {
        qWarning() << "File "<<dbPath<<"doesn't exist";
        exit(1);
    } else {
        dbFile.copy("./"+lessonName);
        dbPath = lessonName;
        QFile::setPermissions(dbPath, QFile::WriteOwner | QFile::ReadOwner);
        qDebug() << lessonName << "is copied into the local folder";
    }
    //QString name("Lesson DB");
    qDebug() << "Try to open" << dbFile.fileName();
    dbServer = QSqlDatabase::addDatabase("QSQLITE", dbPath);
    dbServer.setDatabaseName(dbPath);
    bool ok = dbServer.open();
    if (!ok && dbServer.lastError().isValid())
    {
        qDebug() <<  dbServer.lastError();
        exit(1);
    }

    qDebug() << "Database "<<lessonName<< " is open:"<<ok;

    curQuery = QSqlQuery(dbServer);
    curQuery.prepare("select * from match_dict limit :num;");
    //numberOfWordsPerLesson should be checked
    curQuery.bindValue(":num", numberOfWordsPerLesson);
    curQuery.exec();

    if (curQuery.isActive())
    {
        int fieldWord1 = curQuery.record().indexOf("word1");
        int fieldWord2 = curQuery.record().indexOf("word2");
        lessonWords = QVector < QPair<QString, QString> > (numberOfWordsPerLesson);
        int i = 0;

        if (!inverted)
        {
            while (curQuery.next() && i < numberOfWordsPerLesson) {
                lessonWords[i].first = curQuery.value(fieldWord1).toString();
                lessonWords[i].second = curQuery.value(fieldWord2).toString();
                i++;
            }
            //qDebug() << "I am here1"<< currentWordNumber;
        } else
        {
            while (curQuery.next() && i < numberOfWordsPerLesson) {
                lessonWords[i].second = curQuery.value(fieldWord1).toString();
                lessonWords[i].first = curQuery.value(fieldWord2).toString();
                i++;
            }
            //qDebug() << "I am here2";
        }
    }

}

SqlDBServer::~SqlDBServer()
{
    //dbServer.removeDatabase(dbServer.connectionName());
    dbServer.close();
}

void SqlDBServer::closeDB()
{
    dbServer.close();
}

//void DBServer::initDBserver(QString &db)
//{

//}

